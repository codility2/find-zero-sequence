(ns tests
  (:require [clojure.spec.alpha :as spec]
            [clojure.test :refer [is deftest run-tests]]
            [main :refer :all]))

(spec/exercise-fn `has-0-seq? 50)

(spec/def ::no0int (spec/and :main/lim-int #(not= 0 %)))
(def nums-spec (spec/coll-of ::no0int :min-count 10))
(def nums (map first (spec/exercise nums-spec 10)))

(time (doseq [n nums]
        (println (if (has-0-seq? n) 
                   "yes"
                   "no"))))

(deftest middle_seq_found []
 (let [_seq [1 1 -1 2]] 
  (is (= [1 -1] (has-0-seq? _seq)))))

(deftest corner_seq_found []
 (let [_seq [1 2 1 -1]] 
  (is (= [1 -1] (has-0-seq? _seq)))))

(deftest full_seq_found []
 (let [_seq [1 -1]] 
  (is (= _seq (has-0-seq? _seq)))))

(deftest no_seq_found []
 (let [_seq [1 2]] 
  (is (not (has-0-seq? _seq)))))

(run-tests)
