(ns main
    (:require [clojure.spec.alpha :as spec]))

(defn sum-0?[coll]
  (= 0 (apply +' coll)))

(spec/def ::lim-int (spec/and int? #(< -1000000 % 1000000)))
(spec/fdef has-0-seq?
           :args (spec/cat :coll (spec/coll-of ::lim-int))
           :ret boolean?)

(defn has-0-seq? [coll]
  (let [vcoll (vec coll)
        len (count vcoll)]
  (first (filter sum-0? (for [i (range len)
                              j (range (inc i) (inc len))]
                          (subvec vcoll i j))))))

